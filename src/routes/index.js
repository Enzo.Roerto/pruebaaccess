import express, { Router } from "express";
import cors from "cors";
import sqlite3 from "sqlite3";
import { open } from "sqlite";
import axios from "axios";
import db from "../db/database.js";

function routerApi(app) {
  const router = Router();
  app.use("/api/", router);

  router.get("/listaBancos", async function (req, res) {
    try {
      // Realizar la petición GET a una URL externa
      const response = await axios.get(
        "https://random-data-api.com/api/v2/banks?size=100&is_xml=true"
      );
      // Enviar la respuesta recibida desde la URL externa como respuesta a la solicitud en nuestro servidor
      data.forEach((item) => {
        const insertQuery =
          "INSERT INTO bank (uid, account_number, iban, bank_name, routing_number, swift_bic,created_up,updated_up) VALUES (?, ?,?,?,?,?,?,?)";
        db.run(
          insertQuery,
          [
            item.uid,
            item.account_number,
            item.iban,
            item.bank_name,
            item.routing_number,
            item.swift_bic,
            Date.now(),
            Date.now(),
          ],
          function (err) {
            if (err) {
              console.error(err.message);
              db.close((err) => {
                if (err) {
                  console.error(err.message);
                } else {
                  console.log("Closed the database connection.");
                }
              });
              // Manejar el error si es necesario
            } else {
              console.log(`Row inserted with ID: ${this.lastID}`);
            }
          }
        );
      });
      res.json("Informacion Ingresada Localmente");
    } catch (error) {
      // Manejar errores
      res.status(500).json({ error: "Error al realizar la petición" });
    }
  });

  router.post("/CrearBanco", (req, res, next) => {
    const data = req.body;
     const { uid, account_number, iban, bank_name, routing_number, swift_bic } =
      data;

    const insertQuery = `INSERT INTO bank_accounts (uid, account_number, iban, bank_name, routing_number, swift_bic,created_up,updated_up)
                         VALUES (?, ?, ?, ?, ?, ?,?,?)`;

    db.run(
      insertQuery,
      [
        uid,
        account_number,
        iban,
        bank_name,
        routing_number,
        swift_bic,
        Date.now(),
        Date.now(),
      ],
      (err) => {
        if (err) {
          console.error(err.message);
          res
            .status(500)
            .json({ error: "Error al insertar en la base de datos" });
        } else {
          res.json({ message: "Datos insertados correctamente" });
        }
      }
    );
  });

  // Ruta POST para actualizar el campo "bank_name"
  router.post("/actualizarBanco", (req, res) => {
    const { uid, bank_name } = req.body;
    const update = Date.Now();
    const updateQuery = `UPDATE bank_accounts
                       SET bank_name = ?,updated_up =?
                       WHERE id = ?`;

    db.run(updateQuery, [bank_name, update, id], (err) => {
      if (err) {
        console.error(err.message);
        res
          .status(500)
          .json({ error: "Error al actualizar en la base de datos" });
      } else {
        res.json({ message: 'Campo "bank_name" actualizado correctamente' });
      }
    });
  });
}

export default routerApi;
