import express from "express";
import cors from "cors";
import routerApi from './routes/index.js'
import bodyParser from 'body-parser';



const app = express();
const PORT = 3000;
app.use(cors(),express.json());

// Función para abrir la base de datos SQLite

routerApi(app)
app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});
