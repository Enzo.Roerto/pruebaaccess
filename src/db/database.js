import sqlite3 from "sqlite3";
import md5 from "md5";
import { config } from "./config.js";

const DBSOURCE = config.dbPath; // Suponiendo que config.dbPath contiene la ruta a la base de datos

const db = new sqlite3.Database(DBSOURCE, (err) => {
    
  if (err) {
    // Cannot open database
    console.error(err.message);
    throw err;
  } else {
    console.log("Connected to the SQLite database.");
    db.run(
      `CREATE TABLE IF NOT EXISTS bank_accounts (
            id INTEGER PRIMARY KEY,
            uid TEXT NOT NULL,
            account_number TEXT NOT NULL,
            iban TEXT NOT NULL,
            bank_name TEXT NOT NULL,
            routing_number TEXT NOT NULL,
            swift_bic TEXT NOT NULL
        );`,
      (err) => {
        if (err) {
          // Table already created
        } else {
          // Table just created, creating some rows
          console.log("esta creada la tabla");
        }
      }
    );
  }
});

export default db;
